#include "configuraciones.h"
#include "menu/menu.h"
#include "juego/juego_arcanoid.h"
#include "juego/jugador.h"
#include "juego/bloque.h"
#include "juego/pelota.h"

namespace arkanoid
{
	namespace config
	{
		Music musicaMenu;
		Music musicaJuego;
		Sound efectoBloque;
		Sound efectoConfirmar;
		Sound efectoGameOver;
		Sound efectoMouseOver;
		Sound efectoPelotaColision;
		Sound efectoVictoria;
		bool musicas;
		bool efectos;

		Texture2D fondo[3];
		Texture2D lateral;

		Vector2 posCentro;
		bool enPausa = false;
		int vidaInicialJugador = 3;
		bool gameOver = true;
		bool enJuego = true;
		bool mousePulsado = false;
		int resolucionActualIndex = 0;
		int targetFPS = 60;
		float escalado = 1;
		float tiempoescalado;

		Vector2 resoluciones[resolucionesMax] = { 0 };
		Vector2 resolucionActual = { 850, 480 };
		Vector2 mouse;
		
		void preInit()
		{
			InitWindow(config::resolucionActual.x, config::resolucionActual.y, "Arkanoid");

			//--------- CARGA DE SONIDOS ---------
			InitAudioDevice();
			musicaMenu = LoadMusicStream("../res/assets/audio/musica_menu.mp3");
			PlayMusicStream(musicaMenu);
			musicaJuego = LoadMusicStream("../res/assets/audio/musica_ingame.mp3");

			efectoBloque = LoadSound("../res/assets/audio/efecto_bloque.wav");
			efectoConfirmar = LoadSound("../res/assets/audio/efecto_confirmar.wav");
			efectoGameOver = LoadSound("../res/assets/audio/efecto_gameover.wav");
			efectoMouseOver = LoadSound("../res/assets/audio/efecto_mouseover.wav");
			efectoPelotaColision = LoadSound("../res/assets/audio/pelota_colision.wav");
			efectoVictoria = LoadSound("../res/assets/audio/efecto_victoria.wav");
			musicas = true;
			efectos = true;
			config::tiempoescalado = 1;

			//--------- SETEO DE RESOLUCIONES ---------
			config::resoluciones[0] = {  850,  480 };
			config::resoluciones[1] = { 1280,  720 };
			config::resoluciones[2] = { 1600,  900 };
			config::resoluciones[3] = { 1920, 1080 };

			posCentro.x = GetWindowPosition().x + resoluciones[0].x / 2;
			posCentro.y = GetWindowPosition().y + resoluciones[0].y / 2;

			//--------- CARGA DE TEXTURAS ---------
			menu::principal::jugar.textura = LoadTexture("../res/assets/textures/botonJugar.png");
			menu::principal::opciones.textura = LoadTexture("../res/assets/textures/botonOpciones.png");
			menu::principal::creditos.textura = LoadTexture("../res/assets/textures/botonCreditos.png");
			menu::principal::salir.textura = LoadTexture("../res/assets/textures/botonSalir.png");

			jugadores::texturas[0] = LoadTexture("../res/assets/textures/jugador00.png");
			jugadores::texturas[1] = LoadTexture("../res/assets/textures/jugador01.png");
			jugadores::texturas[2] = LoadTexture("../res/assets/textures/jugador02.png");
			jugadores::texturas[3] = LoadTexture("../res/assets/textures/jugador03.png");

			bloques::texturas[0] = LoadTexture("../res/assets/textures/bloque00.png");
			bloques::texturas[1] = LoadTexture("../res/assets/textures/bloque01.png");
			bloques::texturas[2] = LoadTexture("../res/assets/textures/bloque02.png");
			bloques::texturas[3] = LoadTexture("../res/assets/textures/bloque03.png");

			pelotas::texturas[0] = LoadTexture("../res/assets/textures/pelota01.png");
			escenario::panel = LoadTexture("../res/assets/textures/panel.png");
			escenario::panel.height *= 3.5f;
			escenario::panel.width *= 3.5f;

			fondo[0] = LoadTexture("../res/assets/fondo/fondo00.png");
			fondo[1] = LoadTexture("../res/assets/fondo/fondo01.png");
			fondo[2] = LoadTexture("../res/assets/fondo/fondo02.png");
			lateral = LoadTexture("../res/assets/textures/borde.png");
			lateral.height = 500 * escalado;
			lateral.width = 10 * escalado;
			menu::pausa::continuarPartida.textura = LoadTexture("../res/assets/textures/boton01.png");
			menu::pausa::continuarPartida.textura.width *= 2;
			menu::pausa::continuarPartida.textura.height *= 2;
			menu::pausa::volerAlMenu.textura = menu::pausa::continuarPartida.textura;

			menu::opciones::musicasOnOff.textura = menu::pausa::continuarPartida.textura;
			menu::opciones::efectosOnOff.textura = menu::pausa::continuarPartida.textura;
			menu::opciones::fps.textura = menu::pausa::continuarPartida.textura;

			jugadores::jugador.nivel = 1;
		}

		void deInit()
		{
			UnloadTexture(menu::principal::jugar.textura);
			UnloadTexture(menu::principal::opciones.textura);
			UnloadTexture(menu::principal::creditos.textura);
			UnloadTexture(menu::principal::salir.textura);

			UnloadTexture(jugadores::texturas[0]);
			UnloadTexture(jugadores::texturas[1]);
			UnloadTexture(jugadores::texturas[2]);
			UnloadTexture(jugadores::texturas[3]);

			UnloadTexture(bloques::texturas[0]);
			UnloadTexture(bloques::texturas[1]);
			UnloadTexture(bloques::texturas[2]);
			UnloadTexture(bloques::texturas[3]);

			UnloadTexture(pelotas::texturas[0]);
			UnloadTexture(escenario::panel);
			UnloadTexture(menu::pausa::continuarPartida.textura);
			UnloadTexture(menu::pausa::volerAlMenu.textura);

			UnloadTexture(fondo[0]);
			UnloadTexture(fondo[1]);
			UnloadTexture(fondo[2]);
			UnloadTexture(lateral);

			UnloadTexture(menu::opciones::musicasOnOff.textura);
			UnloadTexture(menu::opciones::efectosOnOff.textura);
			UnloadTexture(menu::opciones::fps.textura);

			UnloadMusicStream(musicaMenu);
			UnloadMusicStream(musicaJuego);
			UnloadSound(efectoBloque);
			UnloadSound(efectoConfirmar);
			UnloadSound(efectoGameOver);
			UnloadSound(efectoMouseOver);
			UnloadSound(efectoPelotaColision);
			UnloadSound(efectoVictoria);
		}
		void init()
		{
			//--------- OTRAS L�GICAS ---------
			enJuego = true;
			gameOver = false;

			//--------- SETEO DE PANTALLA ---------
			SetExitKey(NULL);
			SetTargetFPS(targetFPS);
			escalado = config::resoluciones[config::resolucionActualIndex].x / config::resoluciones[0].x;

			//--------- PIXELES DE BOTONES ---------
			menu::principal::jugar.boton.width = 272 * escalado;
			menu::principal::jugar.boton.height = 51 * escalado;

			menu::principal::opciones.boton.width = 272 * escalado;
			menu::principal::opciones.boton.height = 51 * escalado;

			menu::principal::creditos.boton.width = 272 * escalado;
			menu::principal::creditos.boton.height = 51 * escalado;

			menu::principal::salir.boton.width = 219 * escalado;
			menu::principal::salir.boton.height = 78 * escalado;

			for (int i = 0; i < bloques::maxBloques; i++)
			{
				bloques::bloque[i].rec.width = 54 * escalado;
				bloques::bloque[i].rec.height = 22 * escalado;
			}
			
			//--------- ESCALADO DE BOTONES ---------
			menu::principal::jugar.boton.x = resolucionActual.x * 5 / 10 - menu::principal::jugar.boton.width / 2;
			menu::principal::jugar.boton.y = resolucionActual.y * 2 / 10 - menu::principal::jugar.boton.height / 2;

			menu::principal::opciones.boton.x = resolucionActual.x * 5 / 10 - menu::principal::opciones.boton.width / 2;
			menu::principal::opciones.boton.y = resolucionActual.y * 4 / 10 - menu::principal::opciones.boton.height / 2;

			menu::principal::creditos.boton.x = resolucionActual.x * 5 / 10 - menu::principal::creditos.boton.width / 2;
			menu::principal::creditos.boton.y = resolucionActual.y * 6 / 10 - menu::principal::creditos.boton.height / 2;

			menu::principal::salir.boton.x = resolucionActual.x * 8 / 10 - menu::principal::salir.boton.width / 2;
			menu::principal::salir.boton.y = resolucionActual.y * 8 / 10 - menu::principal::salir.boton.height / 2;

			//----------- ESCENARIO -----------
			escenario::izqArri = { (float)GetScreenWidth() * 1 / 4, 0 };
			escenario::derAbaj = { (float)GetScreenWidth() * 3 / 4, (float)GetScreenHeight() };

			escenario::limites[0].rec.x = 0;
			escenario::limites[0].rec.y = 0;
			escenario::limites[0].rec.width = GetScreenWidth() * 1 / 4;
			escenario::limites[0].rec.height = GetScreenHeight();
			escenario::limites[0].color = BLACK;

			escenario::limites[1].rec.x = GetScreenWidth() * 3 / 4;
			escenario::limites[1].rec.y = 0;
			escenario::limites[1].rec.width = GetScreenWidth() * 1 / 4+10;
			escenario::limites[1].rec.height = GetScreenHeight();
			escenario::limites[1].color = BLACK;

			escenario::limites[2].rec.x = 0;
			escenario::limites[2].rec.y = -100;
			escenario::limites[2].rec.width = GetScreenWidth();
			escenario::limites[2].rec.height = 100;
			escenario::limites[2].color = BLACK;

			escenario::limites[3].rec.x = 0;
			escenario::limites[3].rec.y = GetScreenHeight();
			escenario::limites[3].rec.width = GetScreenWidth();
			escenario::limites[3].rec.height = 100;
			escenario::limites[3].color = BLACK;

			escenario::panelPos = { (float)(GetScreenWidth() / 2 - escenario::panel.width / 2 * escalado), (float)(GetScreenHeight() / 2 - escenario::panel.height / 2 * escalado) };

			menu::pausa::continuarPartida.boton.width = menu::pausa::continuarPartida.textura.width * escalado;
			menu::pausa::continuarPartida.boton.height = menu::pausa::continuarPartida.textura.height * escalado;
			menu::pausa::volerAlMenu.boton.width = menu::pausa::volerAlMenu.textura.width * escalado;
			menu::pausa::volerAlMenu.boton.height = menu::pausa::volerAlMenu.textura.height * escalado;

			menu::pausa::volerAlMenu.boton.x = escenario::panelPos.x + (escenario::panel.width * escalado) * 35 / 100 - menu::pausa::volerAlMenu.boton.width / 2;
			menu::pausa::volerAlMenu.boton.y = escenario::panelPos.y + (escenario::panel.height * escalado) / 2 - menu::pausa::volerAlMenu.boton.height / 2;

			menu::pausa::continuarPartida.boton.x = escenario::panelPos.x + (escenario::panel.width * escalado) * 65 / 100 - menu::pausa::continuarPartida.boton.width / 2;
			menu::pausa::continuarPartida.boton.y = escenario::panelPos.y + (escenario::panel.height * escalado) / 2 - menu::pausa::continuarPartida.boton.height / 2;
		
			menu::opciones::musicasOnOff.boton.width = menu::opciones::musicasOnOff.textura.width * escalado;
			menu::opciones::musicasOnOff.boton.height = menu::opciones::musicasOnOff.textura.height * escalado;
			menu::opciones::musicasOnOff.boton.x = GetScreenWidth() * 6 / 10;
			menu::opciones::musicasOnOff.boton.y = GetScreenHeight() * 4 / 10;

			menu::opciones::efectosOnOff.boton.width = menu::opciones::efectosOnOff.textura.width * escalado;
			menu::opciones::efectosOnOff.boton.height = menu::opciones::efectosOnOff.textura.height * escalado;
			menu::opciones::efectosOnOff.boton.x = GetScreenWidth() * 6 / 10;
			menu::opciones::efectosOnOff.boton.y = GetScreenHeight() * 6 / 10;

			menu::opciones::fps.boton.width = menu::pausa::continuarPartida.textura.width * escalado;
			menu::opciones::fps.boton.height = menu::pausa::continuarPartida.textura.height * escalado;
			menu::opciones::fps.boton.x = GetScreenWidth() * 5 / 10;
			menu::opciones::fps.boton.y = GetScreenHeight() * 8 / 10;

			menu::opciones::fpsUp.boton.width = menu::opciones::fps.boton.width / 2;
			menu::opciones::fpsUp.boton.height = menu::opciones::fps.boton.height;
			menu::opciones::fpsUp.boton.x = menu::opciones::fps.boton.x + menu::opciones::fps.boton.width / 2;
			menu::opciones::fpsUp.boton.y = menu::opciones::fps.boton.y;

			menu::opciones::fpsDown.boton.width = menu::opciones::fps.boton.width / 2;
			menu::opciones::fpsDown.boton.height = menu::opciones::fps.boton.height;
			menu::opciones::fpsDown.boton.x = menu::opciones::fps.boton.x;
			menu::opciones::fpsDown.boton.y = menu::opciones::fps.boton.y;
		}

		void cambiarResolucion()
		{
			if (IsKeyPressed(KEY_LEFT))
			{
				resolucionActualIndex--;
				if (resolucionActualIndex < 0)
				{
					resolucionActualIndex = 0;
				}
				else
				{
					resolucionActual = resoluciones[resolucionActualIndex];
					SetWindowSize(resolucionActual.x, resolucionActual.y);
					config::init();

					SetWindowPosition(posCentro.x - resoluciones[resolucionActualIndex].x / 2, posCentro.y - resoluciones[resolucionActualIndex].y / 2);
				}
			}
			else if (IsKeyPressed(KEY_RIGHT))
			{
				resolucionActualIndex++;
				if (resolucionActualIndex > resolucionesMax - 1)
				{
					resolucionActualIndex = resolucionesMax - 1;
				}
				else
				{
					resolucionActual = resoluciones[resolucionActualIndex];
					SetWindowSize(resolucionActual.x, resolucionActual.y);
					config::init();
					
					SetWindowPosition(posCentro.x - resoluciones[resolucionActualIndex].x / 2, posCentro.y - resoluciones[resolucionActualIndex].y / 2);
				}
			}
		}
	}
}
#ifndef CONFIGURACIONES_H
#define CONFIGURACIONES_H

#include "raylib.h"

namespace arkanoid
{
	namespace config
	{
		extern Music musicaMenu;
		extern Music musicaJuego;
		extern Sound efectoBloque;
		extern Sound efectoConfirmar;
		extern Sound efectoGameOver;
		extern Sound efectoMouseOver;
		extern Sound efectoPelotaColision;
		extern Sound efectoVictoria;
		extern bool musicas;
		extern bool efectos;

		extern Texture2D fondo[3];
		extern Texture2D lateral;

		const int resolucionesMax = 4;
		const int bloqueIndestructible = 9;
		extern int vidaInicialJugador;
		extern Vector2 posCentro;
		extern bool enPausa;
		extern bool gameOver;
		extern bool enJuego;
		extern bool mousePulsado;
		extern int resolucionActualIndex;
		extern int targetFPS;
		extern float escalado;
		extern float tiempoescalado;

		extern Vector2 resoluciones[resolucionesMax];
		extern Vector2 resolucionActual;
		extern Vector2 mouse;
		void preInit();
		void deInit();
		void init();
		void cambiarResolucion();
	}
}

#endif
#include "menu.h"
#include <iostream>
#include "configuraciones/configuraciones.h"
#include "juego/juego_arcanoid.h"
#include "juego/nivel.h"
#include "juego/jugador.h"

namespace arkanoid
{
	namespace menu
	{
		MENUACTUAL menuActual[(int)MENUACTUAL::SALIR];
		int menuInmersion = 0;
		namespace principal
		{
			BOTON jugar;
			BOTON opciones;
			BOTON creditos;
			BOTON salir;

			void update()
			{
				if (config::musicas)
					UpdateMusicStream(config::musicaMenu);

				config::cambiarResolucion();
				config::mouse = GetMousePosition();
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					if (CheckCollisionPointRec(config::mouse, jugar.boton))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENUACTUAL::ARKANOID;
						juego::init();
					}
					else if (CheckCollisionPointRec(config::mouse, opciones.boton))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENUACTUAL::OPCIONES;
					}
					else if (CheckCollisionPointRec(config::mouse, creditos.boton))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENUACTUAL::CREDITOS;
					}
					else if (CheckCollisionPointRec(config::mouse, salir.boton))
					{
						menuInmersion++;
						menuActual[menuInmersion] = MENUACTUAL::SALIR;
					}
				}
				draw();
			}

			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);

				DrawTextureEx(config::fondo[0], { -150, 0 }, 0, config::escalado, WHITE);
				DrawTextureEx(config::fondo[1], { config::mouse.x / 10 - 150, 0 }, 0, config::escalado, WHITE);
				
				DrawTextureEx(jugar.textura, { jugar.boton.x, jugar.boton.y }, 0, config::escalado, WHITE);
				DrawTextureEx(opciones.textura, { opciones.boton.x, opciones.boton.y }, 0, config::escalado, WHITE);
				DrawTextureEx(creditos.textura, { creditos.boton.x, creditos.boton.y }, 0, config::escalado, WHITE);
				DrawTextureEx(salir.textura, { salir.boton.x, salir.boton.y }, 0, config::escalado, WHITE);

				EndDrawing();
			}
		}

		namespace opciones
		{
			BOTON musicasOnOff;
			Color colorMusica = GREEN;
			BOTON efectosOnOff;
			Color colorEfectos = GREEN;
			BOTON fps;
			BOTON fpsUp;
			BOTON fpsDown;

			void update()
			{
				if (config::musicas)
					UpdateMusicStream(config::musicaMenu);

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					config::mouse = GetMousePosition();
					if (CheckCollisionPointRec(config::mouse, musicasOnOff.boton))
					{
						config::musicas = !config::musicas;
					}
					else if (CheckCollisionPointRec(config::mouse, efectosOnOff.boton))
					{
						config::efectos = !config::efectos;
					}
					else if (CheckCollisionPointRec(config::mouse, fpsUp.boton))
					{
						config::targetFPS += 10;
						if (config::targetFPS > 100)
							config::targetFPS = 100;
						//SetTargetFPS(config::targetFPS);
						config::tiempoescalado = 60 / config::targetFPS;
					}
					else if (CheckCollisionPointRec(config::mouse, fpsDown.boton))
					{
						config::targetFPS -= 10;
						if (config::targetFPS < 30)
							config::targetFPS = 30;
						//SetTargetFPS(config::targetFPS);
						config::tiempoescalado = 60 / config::targetFPS;
					}

					(config::musicas) ? colorMusica = GREEN : colorMusica = RED;
					(config::efectos) ? colorEfectos = GREEN : colorEfectos = RED;
				}
				else if (IsKeyPressed(jugadores::jugador.teclaConfirmar) || IsKeyPressed(jugadores::jugador.teclaPausa))
				{
					menu::menuInmersion--;
				}
				draw();
			}
			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);

				DrawTextureEx(config::fondo[0], { -150, 0 }, 0, config::escalado, WHITE);
				DrawTextureEx(config::fondo[1], { config::mouse.x / 10 - 150, 0 }, 0, config::escalado, WHITE);

				DrawTextureEx(musicasOnOff.textura, { musicasOnOff.boton.x, musicasOnOff.boton.y }, 0, config::escalado, colorMusica);
				DrawText("Musica: ", musicasOnOff.boton.x - MeasureText("Musica: ", 30 * config::escalado), musicasOnOff.boton.y, 30 * config::escalado, BLACK);
				
				DrawTextureEx(efectosOnOff.textura, { efectosOnOff.boton.x, efectosOnOff.boton.y }, 0, config::escalado, colorEfectos);
				DrawText("Efectos: ", efectosOnOff.boton.x - MeasureText("Efectos: ", 30 * config::escalado), efectosOnOff.boton.y, 30 * config::escalado, BLACK);

				DrawTextureEx(fps.textura, { fps.boton.x, fps.boton.y }, 0, config::escalado, PINK);
				if (config::targetFPS == 30)
					DrawText("fps: 30", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 40)
					DrawText("fps: 40", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 50)
					DrawText("fps: 50", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 60)
					DrawText("fps: 60", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 70)
					DrawText("fps: 70", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 80)
					DrawText("fps: 80", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 90)
					DrawText("fps: 90", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else if (config::targetFPS == 100)
					DrawText("fps:100", fps.boton.x + 2, fps.boton.y + 10, 30 * config::escalado, BLACK);
				else
					DrawText("FPS", fps.boton.x , fps.boton.y + 10, 30 * config::escalado, BLACK);

				EndDrawing();
			}
		}
		namespace creditos
		{
			void update()
			{
				if (config::musicas)
					UpdateMusicStream(config::musicaMenu);
				if (IsKeyPressed(KEY_ONE) || IsKeyPressed(KEY_KP_1))
				{
					OpenURL("https://trello.com/b/OOsEeyG2/dsvj12020olivearkanoid");
				}
				else if (IsKeyPressed(KEY_TWO) || IsKeyPressed(KEY_KP_2))
				{
					OpenURL("https://gitlab.com/federicoolive/dsvj1_2020_pong/-/tree/master/");
				}
				else if (IsKeyPressed(KEY_THREE) || IsKeyPressed(KEY_KP_3))
				{
					OpenURL("https://freesound.org/people/InspectorJ/sounds/403006/");
					OpenURL("https://freesound.org/people/newlocknew/sounds/515837/");
					OpenURL("https://freesound.org/people/VABsounds/sounds/403920/");
					OpenURL("https://freesound.org/people/edwardszakal/sounds/514154/");
					OpenURL("https://freesound.org/people/ckvoiceover/sounds/401341/");
					OpenURL("https://freesound.org/people/Leszek_Szary/sounds/133283/");
					OpenURL("https://freesound.org/people/BucketManDave/sounds/491677/");
					OpenURL("https://freesound.org/people/Sheyvan/sounds/470083/");

				}
				else if (IsKeyPressed(GetKeyPressed()) || IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					menuInmersion--;
				}
				draw();
			}
			void draw()
			{
				BeginDrawing();
				ClearBackground(WHITE);
				DrawText("Programer & Game Designer: Federico Olive.  -  Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Programer & Game Designer: Federico Olive.", 15 * config::escalado), 100 * config::escalado, 15 * config::escalado, RED);
				DrawText("UI: Santiago Laffosse.  -  Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("UI: Santiago Laffosse.", 15 * config::escalado),											150 * config::escalado, 15 * config::escalado, RED);
				DrawText("Enviroment Artist: Joel Montillo.  -  Ex-Estudiante de Image Campus, AR.", GetScreenWidth() / 2 - MeasureText("Enviroment Artist: Joel Montillo.", 15 * config::escalado),				200 * config::escalado, 15 * config::escalado, RED);
				
				DrawText("Precione 1 para abrir Trello.", GetScreenWidth() / 2 - MeasureText("Precione 1 para abrir Trello.", 15 * config::escalado),																300 * config::escalado, 15 * config::escalado, BLUE);
				DrawText("Precione 2 para abrir GitLab.", GetScreenWidth() / 2 - MeasureText("Precione 2 para abrir GitLab.", 15 * config::escalado),																350 * config::escalado, 15 * config::escalado, BLUE);
				DrawText("Precione 3 para abrir referencias de asset.", GetScreenWidth() / 2 - MeasureText("Precione x para abrir xxxxxx.", 15 * config::escalado),									400 * config::escalado, 15 * config::escalado, BLUE);

				DrawText("Version 1.0", 0, GetScreenHeight() - 20, 15, RED);
				EndDrawing();
			}
		}
		namespace salir
		{
			void update()
			{
				config::enJuego = false;
			}
		}

		namespace pausa
		{
			BOTON continuarPartida;
			BOTON volerAlMenu;

			void update()
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					config::mouse = GetMousePosition();
					if (CheckCollisionPointRec(config::mouse, continuarPartida.boton))
					{
						config::enPausa = false;
						if (config::gameOver)
						{
							juego::init();
						}
					}
					else if (CheckCollisionPointRec(config::mouse, volerAlMenu.boton))
					{
						menuInmersion--;
					}
				}
			}
		}
	}
}
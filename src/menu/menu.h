#ifndef MENU_H
#define MENU_H

#include "raylib.h"

namespace arkanoid
{
	namespace menu
	{
		struct BOTON
		{
			Rectangle boton;
			Texture2D textura;
		};
		enum class MENUACTUAL { MENUPRINCIPAL,ARKANOID,OPCIONES,CREDITOS,SALIR };
		extern MENUACTUAL menuActual[(int)MENUACTUAL::SALIR];
		extern int menuInmersion;

		namespace principal
		{
			extern BOTON jugar;
			extern BOTON opciones;
			extern BOTON creditos;
			extern BOTON salir;

			void update();
			void draw();
		}
		namespace opciones
		{
			extern BOTON musicasOnOff;
			extern BOTON efectosOnOff;
			extern BOTON fps;
			extern BOTON fpsUp;
			extern BOTON fpsDown;

			void update();
			void draw();
		}
		namespace creditos
		{
			void update();
			void draw();
		}
		namespace salir
		{
			void update();
		}
		namespace pausa
		{
			extern BOTON continuarPartida;
			extern BOTON volerAlMenu;

			void update();
		}
	}
}



#endif
#ifndef PELOTA_H
#define PELOTA_H

#include "raylib.h"

namespace arkanoid
{
	namespace pelotas
	{
		const int maxPelotas = 10;
		const int maxTexturas = 5;
		const Vector2 velInicialPelotas = { 5, -5 };
		struct PELOTA
		{
			bool activo;
			Vector2 pos;
			int radio;
			int rotacion;
			Vector2 tam;
			Vector2 vel;
			Texture2D textura;
			Color color;
		};

		extern PELOTA pelota[maxPelotas];
		extern Texture2D texturas[maxTexturas];
		void init();
	}
}

#endif
#include "pelota.h"
#include "configuraciones/configuraciones.h"
#include "jugador.h"

namespace arkanoid
{
	namespace pelotas
	{
		PELOTA pelota[maxPelotas];
		Texture2D texturas[maxTexturas];
		void init()
		{
			for (int i = 0; i < maxPelotas; i++)
			{
				pelota[i].activo = false;
				pelota[i].color = WHITE;
				pelota[i].radio = (pelotas::texturas[0].width / 2) * config::escalado;
				pelota[i].pos = { config::resolucionActual.x / 2, jugadores::jugador.rec.y - pelota[i].radio * 2 };
				pelota[i].textura = texturas[0];
				pelota[i].tam = { 415, 422 };
				pelota[i].vel.y = velInicialPelotas.x * config::escalado;
				pelota[i].vel.y = velInicialPelotas.y * config::escalado;
			}
			pelota[0].activo = true;
			pelota[0].vel = { 0, 0 };
		}
	}
}
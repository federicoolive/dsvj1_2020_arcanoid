#include "juego_arcanoid.h"
#include <math.h>
#include "configuraciones/configuraciones.h"
#include "jugador.h"
#include "bloque.h"
#include "nivel.h"
#include "pelota.h"
#include "menu/menu.h"

namespace arkanoid
{
	namespace juego
	{
		void init()
		{
			PlayMusicStream(config::musicaJuego);
			jugadores::init();
			bloques::init();
			niveles::init();
			pelotas::init();
		}

		void update()
		{
			if (config::musicas)
				UpdateMusicStream(config::musicaJuego);

			if (!config::enPausa && !config::gameOver)
			{
				if (!jugadores::victoria)
				{
					if (jugadores::jugador.enJuego)
					{
						for (int i = 0; i < pelotas::maxPelotas; i++)
						{
							if (pelotas::pelota[i].activo)
							{
								pelotas::pelota[i].pos.x += (pelotas::pelota[i].vel.x * config::escalado);
								pelotas::pelota[i].pos.y += (pelotas::pelota[i].vel.y * config::escalado);
							}
						}
					}
					else
					{
						pelotas::pelota[0].pos.x = jugadores::jugador.rec.x + jugadores::jugador.rec.width / 2;
					}
				}

				input();
				escenario::colisiones();
			}
			else if (!jugadores::victoria)
			{
				menu::pausa::update();
				if (IsKeyPressed(jugadores::jugador.teclaPausa))
				{
					config::enPausa = !config::enPausa;
				}
			}

			if (chequeoVictoria())
			{
				if (jugadores::jugador.nivel < niveles::maxNiveles && !jugadores::victoria)
				{
					PlaySound(config::efectoVictoria);
					jugadores::victoria = true;
				}
			}
			draw();
		}

		void input()
		{
			if (!jugadores::victoria)
			{
				if (IsKeyDown(jugadores::jugador.teclaDerecha))
				{
					jugadores::jugador.rec.x += ((jugadores::jugador.velocidad * config::escalado));
				}
				if (IsKeyDown(jugadores::jugador.teclaIzquierda))
				{
					jugadores::jugador.rec.x -= ((jugadores::jugador.velocidad * config::escalado));
				}

				if (IsKeyPressed(jugadores::jugador.teclaConfirmar))
				{
					if (!jugadores::jugador.enJuego)
					{
						pelotas::pelota[0].pos.x = jugadores::jugador.rec.x + jugadores::jugador.rec.width / 2;
						if (IsKeyPressed(jugadores::jugador.teclaConfirmar))
						{
							pelotas::pelota[0].activo = true;
							jugadores::jugador.enJuego = true;
							pelotas::pelota[0].vel.x = pelotas::velInicialPelotas.x * config::escalado;
							pelotas::pelota[0].vel.y = pelotas::velInicialPelotas.y * config::escalado;
						}
					}
				}
				if (IsKeyPressed(jugadores::jugador.teclaPausa))
				{
					config::enPausa = !config::enPausa;
				}
			}

			if (jugadores::victoria)
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					config::mouse = GetMousePosition();
					if (CheckCollisionPointRec(config::mouse, menu::pausa::continuarPartida.boton))
					{
						jugadores::jugador.nivel++;
						juego::init();
					}
					else if (CheckCollisionPointRec(config::mouse, menu::pausa::volerAlMenu.boton))
					{
						menu::menuInmersion--;
					}
				}
			}
		}
		
		void draw()
		{
			BeginDrawing();
			ClearBackground(WHITE);

			DrawTextureEx(config::fondo[0], { -100, 0 }, 0, config::escalado, WHITE);
			DrawTextureEx(config::fondo[1], { -jugadores::jugador.rec.x / 10 - 100, 0 }, 0, config::escalado, WHITE);
			DrawTextureEx(config::fondo[2], { -jugadores::jugador.rec.x / 2 - 100, 0 }, 0, config::escalado, WHITE);
			for (int i = 0; i < escenario::limitesMax; i++)	// L�mites
			{
				DrawRectangleRec(escenario::limites[i].rec, escenario::limites[i].color);
			}
			DrawTextureEx(config::lateral, { escenario::izqArri.x - config::lateral.width * config::escalado, 0 }, 0, config::escalado, WHITE);
			DrawTextureEx(config::lateral, { escenario::derAbaj.x, 0 }, 0, config::escalado, WHITE);


			for (int i = 0; i < pelotas::maxPelotas; i++)	// Pelotas
			{
				if (pelotas::pelota[i].activo)
				{
					DrawTextureEx(pelotas::pelota[i].textura, { pelotas::pelota[i].pos.x - pelotas::pelota[i].radio,pelotas::pelota[i].pos.y - pelotas::pelota[i].radio }, pelotas::pelota[i].rotacion, config::escalado, WHITE);
				}
			}
			

			for (int i = 0; i < bloques::maxBloques; i++)	// Bloques
			{
				if (bloques::bloque[i].activo)
				{
					DrawTextureEx(bloques::bloque[i].textura, { bloques::bloque[i].rec.x, bloques::bloque[i].rec.y }, 0, config::escalado, bloques::bloque[i].color);
				}
			}

			DrawTextureEx(jugadores::jugador.textura, { jugadores::jugador.rec.x, jugadores::jugador.rec.y }, 0, config::escalado, WHITE);	// Jugador
			dibujarEstadisticas();
			
			if (config::enPausa || config::gameOver)
			{
				DrawTextureEx(escenario::panel, escenario::panelPos, 0, 1 * config::escalado, BLUE);
				DrawTextureEx(menu::pausa::continuarPartida.textura, { menu::pausa::continuarPartida.boton.x, menu::pausa::continuarPartida.boton.y }, 0, 1 * config::escalado, GREEN);
				
				if (config::gameOver)
				{
					DrawText("Reiniciar", menu::pausa::continuarPartida.boton.x + menu::pausa::continuarPartida.boton.width / 2 - MeasureText("Reiniciar", 23 * config::escalado) / 2, menu::pausa::continuarPartida.boton.y + (menu::pausa::continuarPartida.boton.height - 23 * config::escalado) / 2, 23 * config::escalado, WHITE);
				}
				else
				{
					DrawText("Continuar", menu::pausa::continuarPartida.boton.x + menu::pausa::continuarPartida.boton.width / 2 - MeasureText("Continuar", 23 * config::escalado) / 2, menu::pausa::continuarPartida.boton.y + (menu::pausa::continuarPartida.boton.height - 23 * config::escalado) / 2, 23 * config::escalado, WHITE);
				}

				DrawTextureEx(menu::pausa::volerAlMenu.textura, { menu::pausa::volerAlMenu.boton.x, menu::pausa::volerAlMenu.boton.y }, 0, 1 * config::escalado, RED);
				DrawText("Al Menu", menu::pausa::volerAlMenu.boton.x + menu::pausa::volerAlMenu.boton.width / 2 - MeasureText("Al Menu", 25 * config::escalado) / 2, menu::pausa::volerAlMenu.boton.y + (menu::pausa::volerAlMenu.boton.height - 25 * config::escalado) / 2, 25 * config::escalado, WHITE);
			}

			if (!jugadores::jugador.enJuego)
			{
				DrawText("Presione enter para comenzar", GetScreenWidth() / 2 - MeasureText("Presione enter para comenzar", 15 * config::escalado) / 2, pelotas::pelota[0].pos.y + pelotas::pelota[0].radio, 15 * config::escalado, BLACK);
			}

			if (jugadores::victoria)
			{
				panelVictoria();
			}
			EndDrawing();
		}

		void dibujarEstadisticas()
		{
			if (jugadores::jugador.vidas == 3)
			{
				DrawText("Vidas: 3", escenario::derAbaj.x + config::lateral.width * config::escalado * 2, config::lateral.width * config::escalado * 2, 15 * config::escalado, WHITE);
			}
			else if (jugadores::jugador.vidas == 2)
			{
				DrawText("Vidas: 2", escenario::derAbaj.x + config::lateral.width * config::escalado * 2, config::lateral.width * config::escalado * 2, 15 * config::escalado, WHITE);
			}
			else if (jugadores::jugador.vidas == 1)
			{
				DrawText("Vidas: 1", escenario::derAbaj.x + config::lateral.width * config::escalado * 2, config::lateral.width * config::escalado * 2, 15 * config::escalado, WHITE);
			}
			else if (jugadores::jugador.vidas == 0)
			{
				DrawText("Vidas: 0", escenario::derAbaj.x + config::lateral.width * config::escalado * 2, config::lateral.width * config::escalado * 2, 15 * config::escalado, WHITE);
			}
		}

		bool chequeoVictoria()
		{
			for (int i = 0; i < bloques::maxBloques; i++)
			{
				if (bloques::bloque[i].activo && bloques::bloque[i].vidaRestante != 9)
				{
					return false;
				}
			}
			return true;
		}

		void panelVictoria()
		{
			DrawTextureEx(escenario::panel, escenario::panelPos, 0, 1 * config::escalado, GREEN);

			DrawTextureEx(menu::pausa::continuarPartida.textura, { menu::pausa::continuarPartida.boton.x, menu::pausa::continuarPartida.boton.y }, 0, 1 * config::escalado, GREEN);
			DrawText("Siguiente", menu::pausa::continuarPartida.boton.x + menu::pausa::continuarPartida.boton.width / 2 - MeasureText("Siguiente", 23 * config::escalado) / 2, menu::pausa::continuarPartida.boton.y + (menu::pausa::continuarPartida.boton.height - 23 * config::escalado) / 2, 23 * config::escalado, WHITE);
					  
			DrawTextureEx(menu::pausa::volerAlMenu.textura, { menu::pausa::volerAlMenu.boton.x, menu::pausa::volerAlMenu.boton.y }, 0, 1 * config::escalado, RED);
			DrawText("Al Menu", menu::pausa::volerAlMenu.boton.x + menu::pausa::volerAlMenu.boton.width / 2 - MeasureText("Al Menu", 25 * config::escalado) / 2, menu::pausa::volerAlMenu.boton.y + (menu::pausa::volerAlMenu.boton.height - 25 * config::escalado) / 2, 25 * config::escalado, WHITE);
		}
	}

	namespace escenario
	{
		Texture2D panel;
		Vector2 panelPos;
		Vector2 izqArri;
		Vector2 derAbaj;

		void colisiones()
		{
			for (int i = 0; i < pelotas::maxPelotas; i++)
			{
				if (pelotas::pelota[i].activo)
				{
					// Pelotas Vs Jugador
					if (CheckCollisionCircleRec(pelotas::pelota[i].pos, pelotas::pelota[i].radio, jugadores::jugador.rec))
					{
						if (config::efectos)
							PlaySound(config::efectoPelotaColision);
						if (pelotas::pelota[i].vel.y > 0)
						{
							pelotas::pelota[i].vel.y *= -1;
							pelotas::pelota[i].vel.x = (pelotas::pelota[i].pos.x - (jugadores::jugador.rec.x + jugadores::jugador.rec.width / 2)) / (jugadores::jugador.rec.width / 2) * 5;
						}
					}

					// Pelotas Vs Escenario
					if (CheckCollisionCircleRec(pelotas::pelota[i].pos, pelotas::pelota[i].radio, escenario::limites[0].rec) && pelotas::pelota[i].vel.x < 0)		// Izq
					{
						pelotas::pelota[i].vel.x *= -1;
						if (config::efectos)
							PlaySound(config::efectoPelotaColision);
					}
					else if (CheckCollisionCircleRec(pelotas::pelota[i].pos, pelotas::pelota[i].radio, escenario::limites[1].rec) && pelotas::pelota[i].vel.x > 0)	// Der
					{
						pelotas::pelota[i].vel.x *= -1;
						if (config::efectos)
							PlaySound(config::efectoPelotaColision);
					}
					else if (CheckCollisionCircleRec(pelotas::pelota[i].pos, pelotas::pelota[i].radio, escenario::limites[2].rec) && pelotas::pelota[i].vel.y < 0)	// Arri
					{
						pelotas::pelota[i].vel.y *= -1;
						if (config::efectos)
							PlaySound(config::efectoPelotaColision);
					}
					else if (CheckCollisionCircleRec(pelotas::pelota[i].pos, pelotas::pelota[i].radio, escenario::limites[3].rec))									// Abaj
					{
						pelotas::pelota[i].activo = false;
						if (i == 0)
						{
							jugadores::jugador.vidas--;
							if (jugadores::jugador.vidas <= 0)
							{
								config::gameOver = true;
								if (config::efectos)
									PlaySound(config::efectoGameOver);
							}
							else
							{
								jugadores::jugador.enJuego = false;
								pelotas::init();
							}
						}
					}

					// Pelotas Vs Bloques
					for (int j = 0; j < bloques::maxBloques; j++)
					{
						if (bloques::bloque[j].activo)
						{
							if (CheckCollisionCircleRec(pelotas::pelota[i].pos, pelotas::pelota[i].radio, bloques::bloque[j].rec))
							{	
								if (bloques::bloque[j].vidaRestante != 9)
								{
									if (config::efectos)
										PlaySound(config::efectoBloque);
									bloques::bloque[j].vidaRestante--;
									jugadores::jugador.dinero += bloques::bloque[i].recompenza;

									if (bloques::bloque[j].vidaRestante == 0)
									{
										bloques::bloque[j].activo = false;
									}
									else
									{
										if (bloques::bloque[j].vidaInicial - bloques::bloque[j].vidaRestante + 1 < bloques::maxTexturas)
										{
											bloques::bloque[j].textura = bloques::texturas[bloques::bloque[j].vidaInicial - bloques::bloque[j].vidaRestante + 1];
										}
										bloques::setearColorBloque(j);
									}
								}
								else
								{
									if (config::efectos)
										PlaySound(config::efectoPelotaColision);
								}

								if (pelotas::pelota[i].pos.y < bloques::bloque[j].rec.y)											//  -Arriba-
								{
									if (pelotas::pelota[i].vel.y > 0)
									{
										if (pelotas::pelota[i].pos.x < bloques::bloque[j].rec.x && pelotas::pelota[i].vel.x > 0)	// & Izquierda
										{
											pelotas::pelota[i].vel.y = -fabsf(pelotas::pelota[i].vel.y);
											pelotas::pelota[i].vel.x = -fabsf(pelotas::pelota[i].vel.x);
										}
										else if (pelotas::pelota[i].pos.x > bloques::bloque[j].rec.x + bloques::bloque[j].rec.width && pelotas::pelota[i].vel.x < 0)// & Derecha
										{
											pelotas::pelota[i].vel.y = fabsf(pelotas::pelota[i].vel.y);
											pelotas::pelota[i].vel.x = -fabsf(pelotas::pelota[i].vel.x);
										}
										else																						// & Medio
										{
											pelotas::pelota[i].vel.y = -fabsf(pelotas::pelota[i].vel.y);
										}
									}
								}	
								else if (pelotas::pelota[i].pos.y > bloques::bloque[j].rec.y + bloques::bloque[j].rec.height)		//  -Abajo-
								{
									if (pelotas::pelota[i].vel.y < 0)
									{
										if (pelotas::pelota[i].pos.x < bloques::bloque[j].rec.x && pelotas::pelota[i].vel.x > 0)									// & Izquierda
										{
											pelotas::pelota[i].vel.y = fabsf(pelotas::pelota[i].vel.y);
											pelotas::pelota[i].vel.x = -fabsf(pelotas::pelota[i].vel.x);
										}
										else if (pelotas::pelota[i].pos.x > bloques::bloque[j].rec.x + bloques::bloque[j].rec.width && pelotas::pelota[i].vel.x < 0)// & Derecha
										{
											pelotas::pelota[i].vel.y = fabsf(pelotas::pelota[i].vel.y);
											pelotas::pelota[i].vel.x = fabsf(pelotas::pelota[i].vel.x);
										}
										else																						// & Medio
										{
											pelotas::pelota[i].vel.y = -(pelotas::pelota[i].vel.y);
										}
									}
								}
								else
								{
									if (pelotas::pelota[i].pos.x < bloques::bloque[j].rec.x + bloques::bloque[j].rec.width)
									{
										pelotas::pelota[i].vel.x = -(pelotas::pelota[i].vel.x);
									}
									else
									{
										pelotas::pelota[i].vel.x = -(pelotas::pelota[i].vel.x);
									}
								}
							}
						}
					}
				}				
			}
			// Jugador Vs Escenario
			if (jugadores::jugador.rec.x < escenario::limites[0].rec.x + escenario::limites[0].rec.width)
			{
				jugadores::jugador.rec.x = escenario::limites[0].rec.x + escenario::limites[0].rec.width;
			}
			else if (jugadores::jugador.rec.x + jugadores::jugador.rec.width > escenario::limites[1].rec.x)
			{
				jugadores::jugador.rec.x = escenario::limites[1].rec.x - jugadores::jugador.rec.width;
			}
		}

		ESCENARIO limites[limitesMax];
	}
}
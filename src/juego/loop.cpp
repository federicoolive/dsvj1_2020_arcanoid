#include "loop.h"
#include <time.h>
#include <iostream>
#include "configuraciones/configuraciones.h"
#include "menu/menu.h"
#include "juego_arcanoid.h"
#include "jugador.h"

namespace arkanoid
{
	namespace juego
	{
		int reloj[3] = { 0 };
		void jugar()
		{
			config::preInit();

			config::init();

			while (!WindowShouldClose() && config::enJuego)		// Game Loop
			{
				reloj[0] = clock();
				switch (menu::menuActual[menu::menuInmersion])
				{
				case menu::MENUACTUAL::MENUPRINCIPAL:

					menu::principal::update();

					break;
				case menu::MENUACTUAL::ARKANOID:

					juego::update();

					break;
				case menu::MENUACTUAL::OPCIONES:

					menu::opciones::update();

					break;
				case menu::MENUACTUAL::CREDITOS:

					menu::creditos::update();

					break;
				case menu::MENUACTUAL::SALIR:

					menu::salir::update();

					break;
				default:
					break;
				}
			}
			config::deInit();
			CloseWindow();
		}
	}
}
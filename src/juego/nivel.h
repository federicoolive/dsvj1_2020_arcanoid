#ifndef NIVEL_H
#define NIVEL_H

#include "raylib.h"

namespace arkanoid
{
	namespace niveles
	{
		const int maxNiveles = 10;
		const int bloquesMaxFila = 8;
		extern int bajarBloques;
		extern int tiempoBajada;
		void init();
		void cargarNivel01();
		void cargarNivel02();
		void cargarNivel03();
		void cargarNivel04();
		void cargarNivel05();
		void cargarNivel06();
		void cargarNivel07();
		void cargarNivel08();
		void cargarNivel09();
		void cargarNivel10();
		void cargarNivelGen();
		void terminarNivel();
	}
}

#endif
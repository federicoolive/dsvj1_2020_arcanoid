#ifndef JUEGO_ARKANOID_H
#define JUEGO_ARKANOID_H

#include "raylib.h"

namespace arkanoid
{
	namespace juego
	{
		extern int reloj[3];
		struct JUGADOR
		{
			Rectangle rec;
			int velocidad;
			int vidas;

			Texture2D textura;
		};

		extern JUGADOR jugador;
		void init();
		void update();
		void input();
		void draw();
		void dibujarEstadisticas();
		bool chequeoVictoria();
		void panelVictoria();
	}

	namespace escenario
	{
		const int limitesMax = 4;
		extern Texture2D panel;
		extern Vector2 panelPos;
		struct ESCENARIO
		{
			Rectangle rec;
			Color color;
		};
		extern Vector2 izqArri;
		extern Vector2 derAbaj;
		extern ESCENARIO limites[limitesMax];
		void colisiones();
	}
}


#endif
#ifndef JUGADOR_H
#define JUGADOR_H

#include "raylib.h"

namespace arkanoid
{
	namespace jugadores
	{
		struct JUGADOR
		{
			bool enJuego;
			Rectangle rec;
			int ancho;
			int alto;
			float velocidad;

			int teclaDerecha = KEY_RIGHT;
			int teclaIzquierda = KEY_LEFT;
			int teclaArriba = KEY_UP;
			int teclaAbajo = KEY_DOWN;
			int teclaConfirmar = KEY_ENTER;
			int teclaPausa = KEY_ESCAPE;

			Texture2D textura;

			int nivel;
			int vidas;
			int dinero;
		};
		extern bool victoria;
		extern JUGADOR jugador;
		extern Texture2D texturas[5];

		void init();
	}
}
#endif

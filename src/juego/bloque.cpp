#include "bloque.h"
#include <iostream>
#include "configuraciones/configuraciones.h"
#include "jugador.h"

namespace arkanoid
{
	namespace bloques
	{
		int bloquesActivos = 0;
		BLOQUE bloque[maxBloques];
		Texture2D texturas[maxTexturas];

		int espacioBloquesH = 10;
		int espacioBloquesV = 10;
		void init()
		{
			for (int i = 0; i < maxBloques; i++)
			{
				bloque[i].activo = false;
			}
		}

		void asignarBloque(int i, int vida, int x, int y)
		{
			if (vida < 1) vida = 1;
			else if (vida > 9) vida = 9;

			if (i >= 0 && i < maxBloques)
			{
				bloque[i].activo = true;
				bloque[i].vidaInicial = vida;
				bloque[i].vidaRestante = vida;
				bloque[i].rec.x = x;
				bloque[i].rec.y = y;
				if (vida == 9)
				{
					bloque[i].color = GRAY;
					bloque[i].textura = texturas[0];
				}
				else
				{
					bloque[i].textura = texturas[1];
					setearColorBloque(i);
				}
			}
			else
				std::cout << "No creado. Rompe parametros";
		}
		void setearColorBloque(int iterador)
		{
			switch (bloques::bloque[iterador].vidaRestante)
			{
			case 1:
				bloque[iterador].color = BLUE;
				break;
			case 2:
				bloque[iterador].color = YELLOW;
				break;
			case 3:
				bloque[iterador].color = RED;
				break;
			case 4:
				bloque[iterador].color = GREEN;
				break;
			case 5:
				bloque[iterador].color = DARKGREEN;
				break;
			case 6:
				bloque[iterador].color = PURPLE;
				break;
			case 7:
				bloque[iterador].color = DARKPURPLE;
				break;
			case 8:
				bloque[iterador].color = BROWN;
				break;
			case 9:
				bloque[iterador].color = GRAY;
				break;
			default:
				break;
			}
		}
	}
}

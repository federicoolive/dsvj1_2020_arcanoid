#include "nivel.h"
#include "bloque.h"
#include "jugador.h"
#include "juego_arcanoid.h"
#include "configuraciones/configuraciones.h"

namespace arkanoid
{
	namespace niveles
	{
		Vector2 espaciado = { -1, 5 };
		Vector2 pos;
		int vidaBloque = 0;
		int bajarBloques = 0;
		int tiempoBajada = 0;
		
		void init()
		{
			config::gameOver = false;
			config::enPausa = false;
			bajarBloques = 0;
			tiempoBajada = 900;
			switch (jugadores::jugador.nivel)
			{
			case 1:
				cargarNivel01();
				break;
			case 2:
				cargarNivel02();
				break;
			case 3:
				cargarNivel03();
				break;
			case 4:
				cargarNivel04();
				break;
			case 5:
				cargarNivel05();
				break;
			case 6:
				cargarNivel06();
				break;
			case 7:
				cargarNivel07();
				break;
			case 8:
				cargarNivel08();
				break;
			case 9:
				cargarNivel09();
				break;
			case 10:
				cargarNivel10();
				break;
			default:
				cargarNivelGen();
				break;
			}
		}
		void cargarNivel01()
		{
			vidaBloque = 4;
			pos = { escenario::izqArri.x + espaciado.x + 1, escenario::izqArri.y + espaciado.y };
			for (int i = 0; i < 32; i++)
			{
				if (i % 8 == 0 && i != 0)
				{
					pos.y += espaciado.y + bloques::bloque[i].rec.height;
					pos.x = escenario::izqArri.x + espaciado.x + 1;
					vidaBloque--;
				}
				bloques::asignarBloque(i, vidaBloque, pos.x, pos.y);
				bloques::bloque[i].recompenza = 5;
				pos.x += bloques::bloque[i].rec.width + bloques::espaciadoBloques.x;
			}
		}
		void cargarNivel02()
		{
			vidaBloque = 5;
			pos = { escenario::izqArri.x + espaciado.x + 1, escenario::izqArri.y + espaciado.y };
			for (int i = 0; i < 40; i++)
			{
				if (i % 8 == 0 && i != 0)
				{
					pos.y += espaciado.y + bloques::bloque[i].rec.height;
					pos.x = escenario::izqArri.x + espaciado.x + 1;
					vidaBloque--;
				}
				bloques::asignarBloque(i, vidaBloque, pos.x, pos.y);
				bloques::bloque[i].recompenza = 5;
				pos.x += bloques::bloque[i].rec.width + bloques::espaciadoBloques.x;
			}
			
			for (int i = 1; i < 41; i++)
			{
				if (i % 8 == 1 || i % 8 == 0)
					bloques::bloque[i - 1].activo = false;
			}
		}
		void cargarNivel03()
		{
			vidaBloque = 5;
			pos = { escenario::izqArri.x + espaciado.x + 1, escenario::izqArri.y + espaciado.y };
			for (int i = 0; i < 40; i++)
			{
				if (i % 8 == 0 && i != 0)
				{
					pos.y += espaciado.y + bloques::bloque[i].rec.height;
					pos.x = escenario::izqArri.x + espaciado.x + 1;
					vidaBloque--;
				}

				if (i > 30)
					bloques::asignarBloque(i, 9, pos.x, pos.y);
				else
					bloques::asignarBloque(i, vidaBloque, pos.x, pos.y);

				bloques::bloque[i].recompenza = 5;
				pos.x += bloques::bloque[i].rec.width + bloques::espaciadoBloques.x;
			}

			for (int i = 1; i < 41; i++)
			{
				if (i % 8 == 1 || i % 8 == 0)
					bloques::bloque[i - 1].activo = false;
			}
		}
		void cargarNivel04()
		{
			vidaBloque = 4;
			pos = { escenario::izqArri.x + espaciado.x + 1, escenario::izqArri.y + espaciado.y };
			for (int i = 0; i < 32; i++)
			{
				if (i % 8 == 0 && i != 0)
				{
					pos.y += espaciado.y + bloques::bloque[i].rec.height;
					pos.x = escenario::izqArri.x + espaciado.x + 1;
					vidaBloque--;
				}
				if (i % 9 == 0)
				{
					bloques::asignarBloque(i, 9, pos.x, pos.y);
				}
				else if (i % 7 == 0)
				{
					bloques::asignarBloque(i, 6, pos.x, pos.y);
				}
				else if (i % 3 == 0)
				{
					bloques::asignarBloque(i, 4, pos.x, pos.y);
				}
				else
				{
					bloques::asignarBloque(i, 3, pos.x, pos.y);
				}
				bloques::bloque[i].recompenza = 5;
				pos.x += bloques::bloque[i].rec.width + bloques::espaciadoBloques.x;
			}
		}
		void cargarNivel05()
		{
			cargarNivelGen();
		}
		void cargarNivel06()
		{
			cargarNivelGen();
		}
		void cargarNivel07()
		{
			cargarNivelGen();
		}
		void cargarNivel08()
		{
			cargarNivelGen();
		}
		void cargarNivel09()
		{
			cargarNivelGen();
		}
		void cargarNivel10()
		{
			cargarNivelGen();
		}
		void cargarNivelGen()
		{
			pos = { escenario::izqArri.x + espaciado.x + 1, escenario::izqArri.y + espaciado.y };
			int bloquesMax = 8 * GetRandomValue(1, 3) + 3;
			for (int i = 0; i < bloquesMax; i++)
			{
				vidaBloque = GetRandomValue(1, 9);
				if (i % 8 == 0 && i != 0)
				{
					pos.y += espaciado.y + bloques::bloque[i].rec.height;
					pos.x = escenario::izqArri.x + espaciado.x + 1;
				}
				bloques::asignarBloque(i, vidaBloque, pos.x, pos.y);
				bloques::bloque[i].recompenza = 5;
				pos.x += bloques::bloque[i].rec.width + bloques::espaciadoBloques.x;
			}
		}
		void terminarNivel()
		{
			for (int i = 0; i < bloques::maxBloques; i++)
			{
				bloques::bloque[i].activo = false;
			}
		}
	}
}
#include "jugador.h"
#include "configuraciones/configuraciones.h"

namespace arkanoid
{
	namespace jugadores
	{
		JUGADOR jugador;
		Texture2D texturas[5];
		bool victoria;
		void init()
		{
			victoria = false;
			jugador.enJuego = false;
			jugador.rec.width = texturas[0].width * config::escalado;
			jugador.rec.height = texturas[0].height * config::escalado;
			jugador.rec.x = GetScreenWidth() / 2 - jugador.rec.width / 2;
			jugador.rec.y = GetScreenHeight() * 9 / 10;
			jugador.velocidad = 5 * config::escalado;
			jugador.textura = texturas[0];
			jugador.vidas = config::vidaInicialJugador;
		}
	}
}

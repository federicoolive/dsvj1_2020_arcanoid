#ifndef BLOQUE_H
#define BLOQUE_H

#include "raylib.h"

namespace arkanoid
{
	namespace bloques
	{
		const int maxBloques = 50;
		const int maxTexturas = 4;
		const Vector2 espaciadoBloques = { -2,10 };
		extern int bloquesActivos;
		struct BLOQUE
		{
			bool activo;
			Rectangle rec;
			int vidaInicial;
			int vidaRestante;
			Texture2D textura;
			Color color;
			int recompenza;
		};

		extern BLOQUE bloque[maxBloques];
		extern Texture2D texturas[maxTexturas];
		void init();
		void asignarBloque(int i, int vida, int x, int y);
		void setearColorBloque(int i);
	}
}
#endif